#include <iostream>
#include <vector>

template<class T>
void merge(std::vector<T>& array, std::vector<T>& left, std::vector<T>& right) {
size_t i = 0, j = 0, k = 0;
while (i < left.size() && j < right.size()) {
    if (left[i] < right[j]) {
    array[k++] = left[i++];
}       else {
        array[k++] = right[j++];
}
}

        while (i < left.size()) {
        array[k++] = left[i++];
}

        while (j < right.size()) {
        array[k++] = right[j++];
}
}

template<class T>
void mergeSort(std::vector<T>& array) {
if (array.size() <= 1) {
    return;
}

    size_t middle = array.size() / 2;
    std::vector<T> left(array.begin(), array.begin() + middle);
    std::vector<T> right(array.begin() + middle, array.end());

mergeSort(left);
mergeSort(right);
merge(array, left, right);
}

int main() {
std::vector<int> intArray = {4, 2, 5, 1, 3};
mergeSort(intArray);
std::cout << "Sorted integer array: ";
for (int num : intArray) {
    std::cout << num << " ";
}
std::cout << std::endl;

std::vector<double> doubleArray = {3.14, 1.23, 2.71, 0.5};
mergeSort(doubleArray);
std::cout << "Sorted double array: ";
for (double num : doubleArray) {
    std::cout << num << " ";
}
std::cout << std::endl;

std::vector<std::string> stringArray = {"banana", "apple", "cherry", "pear"};
mergeSort(stringArray);
std::cout << "Sorted string array: ";
for (const std::string& str : stringArray) {
    std::cout << str << " ";
}
std::cout << std::endl;

return 0;
}
